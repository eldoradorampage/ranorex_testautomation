﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace madaket_XP001
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The TestCase_5283_new_medicalgroup recording.
    /// </summary>
    [TestModule("472d415c-160d-41a5-a8a9-1f46b4601f23", ModuleType.Recording, 1)]
    public partial class TestCase_5283_new_medicalgroup : ITestModule
    {
        /// <summary>
        /// Holds an instance of the madaket_XP001Repository repository.
        /// </summary>
        public static madaket_XP001Repository repo = madaket_XP001Repository.Instance;

        static TestCase_5283_new_medicalgroup instance = new TestCase_5283_new_medicalgroup();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public TestCase_5283_new_medicalgroup()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static TestCase_5283_new_medicalgroup Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 0;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 0.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.DivTagMedicalGroups' at 68;20.", repo.MadaketHealth.DivTagMedicalGroupsInfo, new RecordItemIndex(0));
            repo.MadaketHealth.DivTagMedicalGroups.Click("68;20");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.MedicalGroupClickNewFlow' at 58;6.", repo.MadaketHealth.MedicalGroupClickNewFlowInfo, new RecordItemIndex(1));
            repo.MadaketHealth.MedicalGroupClickNewFlow.Click("58;6");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.GroupName' at Center.", repo.MadaketHealth.GroupNameInfo, new RecordItemIndex(2));
            repo.MadaketHealth.GroupName.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test_automation_TestCase_5283' with focus on 'MadaketHealth.GroupName'.", repo.MadaketHealth.GroupNameInfo, new RecordItemIndex(3));
            repo.MadaketHealth.GroupName.PressKeys("test_automation_TestCase_5283");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.SiteId' at Center.", repo.MadaketHealth.SiteIdInfo, new RecordItemIndex(4));
            repo.MadaketHealth.SiteId.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'XP001' with focus on 'MadaketHealth.SiteId'.", repo.MadaketHealth.SiteIdInfo, new RecordItemIndex(5));
            repo.MadaketHealth.SiteId.PressKeys("XP001");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.InputTagTin' at Center.", repo.MadaketHealth.InputTagTinInfo, new RecordItemIndex(6));
            repo.MadaketHealth.InputTagTin.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '897644112' with focus on 'MadaketHealth.InputTagTin'.", repo.MadaketHealth.InputTagTinInfo, new RecordItemIndex(7));
            repo.MadaketHealth.InputTagTin.PressKeys("897644112");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.PopupScrollContainer1' at Center.", repo.MadaketHealth.PopupScrollContainer1Info, new RecordItemIndex(8));
            repo.MadaketHealth.PopupScrollContainer1.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Down item 'MadaketHealth.TabIndex0' at Center.", repo.MadaketHealth.TabIndex0Info, new RecordItemIndex(9));
            repo.MadaketHealth.TabIndex0.MoveTo();
            Mouse.ButtonDown(System.Windows.Forms.MouseButtons.Left);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Up item 'MadaketHealth.TabIndex0' at Center.", repo.MadaketHealth.TabIndex0Info, new RecordItemIndex(10));
            repo.MadaketHealth.TabIndex0.MoveTo();
            Mouse.ButtonUp(System.Windows.Forms.MouseButtons.Left);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.SiteId' at Center.", repo.MadaketHealth.SiteIdInfo, new RecordItemIndex(11));
            repo.MadaketHealth.SiteId.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '010101' with focus on 'MadaketHealth.SiteId'.", repo.MadaketHealth.SiteIdInfo, new RecordItemIndex(12));
            repo.MadaketHealth.SiteId.PressKeys("010101");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TabIndex0' at Center.", repo.MadaketHealth.TabIndex0Info, new RecordItemIndex(13));
            repo.MadaketHealth.TabIndex0.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.FirstName' at Center.", repo.MadaketHealth.FirstNameInfo, new RecordItemIndex(14));
            repo.MadaketHealth.FirstName.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test{Tab down}' with focus on 'MadaketHealth.FirstName'.", repo.MadaketHealth.FirstNameInfo, new RecordItemIndex(15));
            repo.MadaketHealth.FirstName.PressKeys("test{Tab down}");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Tab' Up.", new RecordItemIndex(16));
            Keyboard.Up(System.Windows.Forms.Keys.Tab, 15, false);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'automation'.", new RecordItemIndex(17));
            Keyboard.Press("automation");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.Title' at Center.", repo.MadaketHealth.TitleInfo, new RecordItemIndex(18));
            repo.MadaketHealth.Title.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'quality' with focus on 'MadaketHealth.Title'.", repo.MadaketHealth.TitleInfo, new RecordItemIndex(19));
            repo.MadaketHealth.Title.PressKeys("quality");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.Email' at Center.", repo.MadaketHealth.EmailInfo, new RecordItemIndex(20));
            repo.MadaketHealth.Email.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'quality{LShiftKey down}{@ down}{LShiftKey up}quality.com' with focus on 'MadaketHealth.Email'.", repo.MadaketHealth.EmailInfo, new RecordItemIndex(21));
            repo.MadaketHealth.Email.PressKeys("quality{LShiftKey down}{@ down}{LShiftKey up}quality.com");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '617555432110101' with focus on 'MadaketHealth.Phone'.", repo.MadaketHealth.PhoneInfo, new RecordItemIndex(22));
            repo.MadaketHealth.Phone.PressKeys("617555432110101");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '5085554321' with focus on 'MadaketHealth.InputTagFax'.", repo.MadaketHealth.InputTagFaxInfo, new RecordItemIndex(23));
            repo.MadaketHealth.InputTagFax.PressKeys("5085554321");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '100 main street' with focus on 'NewGroupAddress.group_street'.", repo.NewGroupAddress.group_streetInfo, new RecordItemIndex(24));
            repo.NewGroupAddress.group_street.PressKeys("100 main street");
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'boston{Tab down}' with focus on 'NewGroupAddress.group_city'.", repo.NewGroupAddress.group_cityInfo, new RecordItemIndex(25));
            repo.NewGroupAddress.group_city.PressKeys("boston{Tab down}");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.group_state' at Center.", repo.NewGroupAddress.group_stateInfo, new RecordItemIndex(26));
            repo.NewGroupAddress.group_state.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'MA' with focus on 'NewGroupAddress.group_state'.", repo.NewGroupAddress.group_stateInfo, new RecordItemIndex(27));
            repo.NewGroupAddress.group_state.PressKeys("MA");
            
            Report.Screenshot(ReportLevel.Info, "User", "", null, false, new RecordItemIndex(28));
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Focus() on item 'NewGroupAddress.group_zip'.", repo.NewGroupAddress.group_zipInfo, new RecordItemIndex(29));
            repo.NewGroupAddress.group_zip.Focus();
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '02111' on item 'NewGroupAddress.group_zip'.", repo.NewGroupAddress.group_zipInfo, new RecordItemIndex(30));
            repo.NewGroupAddress.group_zip.Element.SetAttributeValue("Value", "02111");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Down item 'MadaketHealth.PopupScrollContainer1' at Center.", repo.MadaketHealth.PopupScrollContainer1Info, new RecordItemIndex(31));
            repo.MadaketHealth.PopupScrollContainer1.MoveTo();
            Mouse.ButtonDown(System.Windows.Forms.MouseButtons.Left);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Up item 'MadaketHealth.PopupScrollContainer1' at Center.", repo.MadaketHealth.PopupScrollContainer1Info, new RecordItemIndex(32));
            repo.MadaketHealth.PopupScrollContainer1.MoveTo();
            Mouse.ButtonUp(System.Windows.Forms.MouseButtons.Left);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 500ms.", new RecordItemIndex(33));
            Delay.Duration(500, false);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'MadaketHealth.TabIndex1'.", repo.MadaketHealth.TabIndex1Info, new RecordItemIndex(34));
            Validate.Exists(repo.MadaketHealth.TabIndex1Info);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TabIndex1' at Center.", repo.MadaketHealth.TabIndex1Info, new RecordItemIndex(35));
            repo.MadaketHealth.TabIndex1.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.SignatureName' at Center.", repo.MadaketHealth.SignatureNameInfo, new RecordItemIndex(36));
            repo.MadaketHealth.SignatureName.Click();
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test automation' with focus on 'MadaketHealth.SignatureName'.", repo.MadaketHealth.SignatureNameInfo, new RecordItemIndex(37));
            repo.MadaketHealth.SignatureName.PressKeys("test automation");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.PopupScrollContainer1' at Center.", repo.MadaketHealth.PopupScrollContainer1Info, new RecordItemIndex(38));
            repo.MadaketHealth.PopupScrollContainer1.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.EDISelect' at Center.", repo.MadaketHealth.EDISelectInfo, new RecordItemIndex(39));
            repo.MadaketHealth.EDISelect.Click();
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 10s to exist. Associated repository item: 'MadaketHealth.Finish'", repo.MadaketHealth.FinishInfo, new ActionTimeout(10000), new RecordItemIndex(40));
            repo.MadaketHealth.FinishInfo.WaitForExists(10000);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Focus() on item 'MadaketHealth.Finish'.", repo.MadaketHealth.FinishInfo, new RecordItemIndex(41));
            repo.MadaketHealth.Finish.Focus();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.Finish' at Center.", repo.MadaketHealth.FinishInfo, new RecordItemIndex(42));
            repo.MadaketHealth.Finish.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.MedicalGroupDetailsSection.TestAutomationTestCase5283' at Center.", repo.MadaketHealth.MedicalGroupDetailsSection.TestAutomationTestCase5283Info, new RecordItemIndex(43));
            repo.MadaketHealth.MedicalGroupDetailsSection.TestAutomationTestCase5283.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.MedicalGroupDetailsSection.DivTagNameLegal' at Center.", repo.MadaketHealth.MedicalGroupDetailsSection.DivTagNameLegalInfo, new RecordItemIndex(44));
            repo.MadaketHealth.MedicalGroupDetailsSection.DivTagNameLegal.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TableLayoutPrimaryEnrollmentAddress.BTagTestAutomation' at Center.", repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.BTagTestAutomationInfo, new RecordItemIndex(45));
            repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.BTagTestAutomation.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321E' at Center.", repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321EInfo, new RecordItemIndex(46));
            repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321E.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag100MainStreet' at Center.", repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag100MainStreetInfo, new RecordItemIndex(47));
            repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag100MainStreet.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.MedicalGroupDetailsSection.DivTag897644112' at Center.", repo.MadaketHealth.MedicalGroupDetailsSection.DivTag897644112Info, new RecordItemIndex(48));
            repo.MadaketHealth.MedicalGroupDetailsSection.DivTag897644112.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.MedicalGroupDetailsSection.DivTag010101' at Center.", repo.MadaketHealth.MedicalGroupDetailsSection.DivTag010101Info, new RecordItemIndex(49));
            repo.MadaketHealth.MedicalGroupDetailsSection.DivTag010101.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth' at 1097;405.", repo.MadaketHealth.SelfInfo, new RecordItemIndex(50));
            repo.MadaketHealth.Self.Click("1097;405");
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321E' at Center.", repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321EInfo, new RecordItemIndex(51));
            repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag6175554321E.Click();
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag5085554321' at Center.", repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag5085554321Info, new RecordItemIndex(52));
            repo.MadaketHealth.TableLayoutPrimaryEnrollmentAddress.DivTag5085554321.Click();
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
