﻿/*
 * Created by Ranorex
 * User: JohnPloumis
 * Date: 10/26/2018
 * Time: 2:31 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of UserCodeModule1.
    /// </summary>
    [TestModule("4140D7DC-0D9E-4CAD-908D-464567973017", ModuleType.UserCode, 1)]
    public class NoData_NewPayer : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public NoData_NewPayer()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 0;
        }
        
        public static void NewPayer_No_Data()
        {
        	Report.Info("Beginning New Payer No Data Test");
        	try
        	{
        		var repo = madaket_XP001.madaket_XP001Repository.Instance;
        		repo.MadaketHealth.DivTagPayers.Click();
        		
        		repo.MadaketHealth.linkNewPayer.Click();
        		repo.MadaketHealth.PopupScrollContainer.Click();
        		repo.MadaketHealth.PopupScrollContainer.Click(Location.LowerRight);
        		repo.MadaketHealth.btnSavePayor.Click();
        		repo.MadaketHealth.errMsgPleaseEnterPayerName.Click();
        		repo.MadaketHealth.HttpsTestMadakethealthComServices.Click("18;22");
        	}
        	catch(Exception ex)
        	{
        		Report.Warn("No Data Payer Exception: " + ex.Message);
        	}
        	Report.Info("Test Complete");
        }
    }
}
