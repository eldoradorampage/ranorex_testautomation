﻿/*
 * Created by Ranorex
 * User: JohnPloumis
 * Date: 11/1/2018
 * Time: 1:59 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using System.IO;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of Fetch_TestData_Directory.
    /// </summary>
    [TestModule("2676ACF6-35F0-4C96-9BD5-5FBBC4117574", ModuleType.UserCode, 1)]
    public class Fetch_TestData_Directory : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Fetch_TestData_Directory()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
			
        	string strDir = Directory.GetCurrentDirectory() + @"\TestData";
        	Madaket_Globals.strTestData = strDir.Replace(@"\bin", "");
        	Report.Info(Madaket_Globals.strTestData);
        }
    }
}
