﻿/*
 * Created by Ranorex
 * User: JohnPloumis
 * Date: 10/26/2018
 * Time: 1:46 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of Iteration34.
    /// </summary>
    [TestModule("3B2F7411-7181-4326-9A6D-704DC23459CD", ModuleType.UserCode, 1)]
    public class Iteration34 : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Iteration34()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        public static void Login()
        {
			Report.Log(ReportLevel.Info, "Website", "Opening web site 'https://edi-alpha.madakethealth.com/services/logout' with browser 'IE' in normal mode.", new RecordItemIndex(0));
            Host.Current.OpenBrowser("https://edi-alpha.madakethealth.com/services/logout", "IE", "", false, false, false, false, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.txtUsername' at 34;8.", repo.MadaketHealth.txtUsernameInfo, new RecordItemIndex(1));
            repo.MadaketHealth.txtUsername.Click("34;8");
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to 'jploumis@madakethealth.com' on item 'MadaketHealth.txtUsername'.", repo.MadaketHealth.txtUsernameInfo, new RecordItemIndex(2));
            repo.MadaketHealth.txtUsername.Element.SetAttributeValue("Value", "jploumis@madakethealth.com");
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to 'Monkey1213!' on item 'MadaketHealth.txtPassword'.", repo.MadaketHealth.txtPasswordInfo, new RecordItemIndex(3));
            repo.MadaketHealth.txtPassword.Element.SetAttributeValue("Value", "Monkey1213!");
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'MadaketHealth.btnLogin'.", repo.MadaketHealth.btnLoginInfo, new RecordItemIndex(4));
            Validate.Exists(repo.MadaketHealth.btnLoginInfo);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.btnLogin' at 62;12.", repo.MadaketHealth.btnLoginInfo, new RecordItemIndex(5));
            repo.MadaketHealth.btnLogin.Click("62;12");

        }
    }
}
