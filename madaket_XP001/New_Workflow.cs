﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace madaket_XP001
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The New_Workflow recording.
    /// </summary>
    [TestModule("e3d6bd35-51de-444c-92cd-b1524a6e7366", ModuleType.Recording, 1)]
    public partial class New_Workflow : ITestModule
    {
        /// <summary>
        /// Holds an instance of the madaket_XP001Repository repository.
        /// </summary>
        public static madaket_XP001Repository repo = madaket_XP001Repository.Instance;

        static New_Workflow instance = new New_Workflow();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public New_Workflow()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static New_Workflow Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 500ms.", new RecordItemIndex(0));
            Delay.Duration(500, false);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Focus() on item 'NewGroupAddress.DivTagAdmin'.", repo.NewGroupAddress.DivTagAdminInfo, new RecordItemIndex(1));
            repo.NewGroupAddress.DivTagAdmin.Focus();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.DivTagAdmin' at 15;14.", repo.NewGroupAddress.DivTagAdminInfo, new RecordItemIndex(2));
            repo.NewGroupAddress.DivTagAdmin.Click("15;14");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.AddWorkflow' at 56;3.", repo.NewGroupAddress.AddWorkflowInfo, new RecordItemIndex(3));
            repo.NewGroupAddress.AddWorkflow.Click("56;3");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.WorkFlowName' at 88;5.", repo.NewGroupAddress.WorkFlowNameInfo, new RecordItemIndex(4));
            repo.NewGroupAddress.WorkFlowName.Click("88;5");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test_automation_workflow_2223' with focus on 'NewGroupAddress.WorkFlowName'.", repo.NewGroupAddress.WorkFlowNameInfo, new RecordItemIndex(5));
            repo.NewGroupAddress.WorkFlowName.PressKeys("test_automation_workflow_2223");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.UniqueWorkflowTemplateSearch' at 102;10.", repo.NewGroupAddress.UniqueWorkflowTemplateSearchInfo, new RecordItemIndex(6));
            repo.NewGroupAddress.UniqueWorkflowTemplateSearch.Click("102;10");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Manual' with focus on 'NewGroupAddress.UniqueWorkflowTemplateSearch'.", repo.NewGroupAddress.UniqueWorkflowTemplateSearchInfo, new RecordItemIndex(7));
            repo.NewGroupAddress.UniqueWorkflowTemplateSearch.PressKeys("Manual");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 5s.", new RecordItemIndex(8));
            Delay.Duration(5000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.AvailityOwnerAvailityPortalAetnaP' at 82;20.", repo.NewGroupAddress.AvailityOwnerAvailityPortalAetnaPInfo, new RecordItemIndex(9));
            repo.NewGroupAddress.AvailityOwnerAvailityPortalAetnaP.Click("82;20");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.NextButton' at 47;6.", repo.NewGroupAddress.NextButtonInfo, new RecordItemIndex(10));
            repo.NewGroupAddress.NextButton.Click("47;6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.BodyContent.SpanTagGeneralWorkflowDetails' at 51;5.", repo.NewGroupAddress.BodyContent.SpanTagGeneralWorkflowDetailsInfo, new RecordItemIndex(11));
            repo.NewGroupAddress.BodyContent.SpanTagGeneralWorkflowDetails.Click("51;5");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
