﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace madaket_XP001
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Admin_Workflow_Status recording.
    /// </summary>
    [TestModule("58b95c6d-c51c-4477-b84a-e5371ed62a90", ModuleType.Recording, 1)]
    public partial class Admin_Workflow_Status : ITestModule
    {
        /// <summary>
        /// Holds an instance of the madaket_XP001Repository repository.
        /// </summary>
        public static madaket_XP001Repository repo = madaket_XP001Repository.Instance;

        static Admin_Workflow_Status instance = new Admin_Workflow_Status();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Admin_Workflow_Status()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Admin_Workflow_Status Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 15s to exist. Associated repository item: 'NewGroupAddress.BodyContent.SpanTagTOOLS'", repo.NewGroupAddress.BodyContent.SpanTagTOOLSInfo, new ActionTimeout(15000), new RecordItemIndex(0));
            repo.NewGroupAddress.BodyContent.SpanTagTOOLSInfo.WaitForExists(15000);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking Focus() on item 'NewGroupAddress.BodyContent.SpanTagTOOLS'.", repo.NewGroupAddress.BodyContent.SpanTagTOOLSInfo, new RecordItemIndex(1));
            repo.NewGroupAddress.BodyContent.SpanTagTOOLS.Focus();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.BodyContent.SpanTagTOOLS' at 15;4.", repo.NewGroupAddress.BodyContent.SpanTagTOOLSInfo, new RecordItemIndex(2));
            repo.NewGroupAddress.BodyContent.SpanTagTOOLS.Click("15;4");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Right Click item 'NewGroupAddress.SetStatus' at 42;3.", repo.NewGroupAddress.SetStatusInfo, new RecordItemIndex(3));
            repo.NewGroupAddress.SetStatus.Click(System.Windows.Forms.MouseButtons.Right, "42;3");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'NewGroupAddress.SetStatus1'.", repo.NewGroupAddress.SetStatus1Info, new RecordItemIndex(4));
            Validate.Exists(repo.NewGroupAddress.SetStatus1Info);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'NewGroupAddress.CloneCaseConfig'.", repo.NewGroupAddress.CloneCaseConfigInfo, new RecordItemIndex(5));
            Validate.Exists(repo.NewGroupAddress.CloneCaseConfigInfo);
            Delay.Milliseconds(100);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating NotExists on item 'NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2'.", repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info, new RecordItemIndex(6));
            //Validate.NotExists(repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info);
            //Delay.Milliseconds(100);
            
            //Report.Log(ReportLevel.Info, "Validation", "Validating NotExists on item 'NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2'.", repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info, new RecordItemIndex(7));
            //Validate.NotExists(repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info);
            //Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SetStatus1' at 45;12.", repo.NewGroupAddress.SetStatus1Info, new RecordItemIndex(8));
            repo.NewGroupAddress.SetStatus1.Click("45;12");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.WorkflowStatus' at 5;6.", repo.NewGroupAddress.WorkflowStatusInfo, new RecordItemIndex(9));
            repo.NewGroupAddress.WorkflowStatus.Click("5;6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.UpdateWorkflowStatusButton' at 44;2.", repo.NewGroupAddress.UpdateWorkflowStatusButtonInfo, new RecordItemIndex(10));
            repo.NewGroupAddress.UpdateWorkflowStatusButton.Click("44;2");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 2.5s.", new RecordItemIndex(11));
            Delay.Duration(2500, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.BodyContent.SpanTagTOOLS' at Center.", repo.NewGroupAddress.BodyContent.SpanTagTOOLSInfo, new RecordItemIndex(12));
            repo.NewGroupAddress.BodyContent.SpanTagTOOLS.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2'.", repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info, new RecordItemIndex(13));
            Validate.Exists(repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2'.", repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info, new RecordItemIndex(14));
            Validate.Exists(repo.NewGroupAddress.BodyContent.HttpsEdiAlphaMadakethealthComServ2Info);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SetStatus1' at Center.", repo.NewGroupAddress.SetStatus1Info, new RecordItemIndex(15));
            repo.NewGroupAddress.SetStatus1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.WorkflowStatus1' at Center.", repo.NewGroupAddress.WorkflowStatus1Info, new RecordItemIndex(16));
            repo.NewGroupAddress.WorkflowStatus1.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.UpdateWorkflowStatusButton' at Center.", repo.NewGroupAddress.UpdateWorkflowStatusButtonInfo, new RecordItemIndex(17));
            repo.NewGroupAddress.UpdateWorkflowStatusButton.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
