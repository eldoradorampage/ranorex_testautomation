﻿/*
 * Created by Ranorex
 * User: JohnPloumis
 * Date: 10/26/2018
 * Time: 3:07 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of Logout_Madaket.
    /// </summary>
    [TestModule("2924C5DC-50A4-42BA-82C2-970EE8F385C7", ModuleType.UserCode, 1)]
    public class Logout_Madaket : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Logout_Madaket()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        public static void Logout()
        {
        	Report.Info("Begin Logout");
        	try
        	{
        		var repo = madaket_XP001.madaket_XP001Repository.Instance;
        		repo.MadaketHealth.Logout.Click();
        		repo.MadaketHealth.Email1.Click();
        		repo.MadaketHealth.Password.Click();
        		Keyboard.Press("{None down}");
        		Keyboard.Press(System.Windows.Forms.Keys.F4 | System.Windows.Forms.Keys.Alt, 62, Keyboard.DefaultKeyPressTime, 1, true);
        		Keyboard.Press("{None up}");
        	}
        	catch(Exception ex)
        	{
        		Report.Warn("Logout Exception: " + ex.Message);
        	}
        	Report.Info("Logout Complete");
        }
    }
}
