﻿/*
 * Created by Ranorex
 * User: JohnPloumis
 * Date: 11/1/2018
 * Time: 2:01 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using System.IO;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of Madaket_Globals.
    /// </summary>
    [TestModule("4EDE80DF-AB3D-4DAD-B18A-044FD550F0C5", ModuleType.UserCode, 1)]
    public class Madaket_Globals : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        /// 
        
        public static string strTestData = "";
        public static string strSiteID = "";
        public static string strSiteName = "";
        public static string strPayer = "test_payer_automation_XP002";
        
        public Madaket_Globals()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        public static void Current_Directory()
        {
        	string strDir = "";
        	strDir = Directory.GetCurrentDirectory() + @"\TestData";
        	string strDirTest = strDir.Replace(@"\bin", "");
        	Report.Info(strDirTest);
        	Madaket_Globals.strTestData = strDirTest;
        }
    }
}
