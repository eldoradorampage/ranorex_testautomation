﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    public partial class Add_New_File_To_Library
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void New_Library_File()
        {
			Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.DivTagLibrary' at 20;29.", repo.NewGroupAddress.DivTagLibraryInfo, new RecordItemIndex(0));
            repo.NewGroupAddress.DivTagLibrary.Click("20;29");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.NewLibraryFile' at 32;12.", repo.NewGroupAddress.NewLibraryFileInfo, new RecordItemIndex(1));
            repo.NewGroupAddress.NewLibraryFile.Click("32;12");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.OwnerTypeTradingPartner' at 5;7.", repo.NewGroupAddress.OwnerTypeTradingPartnerInfo, new RecordItemIndex(2));
            repo.NewGroupAddress.OwnerTypeTradingPartner.Click("5;7");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute TagValue to '6cc2a2b5-b4b3-4166-b2e6-eb7a9809fe07' on item 'NewGroupAddress.FileOwner'.", repo.NewGroupAddress.FileOwnerInfo, new RecordItemIndex(3));
            repo.NewGroupAddress.FileOwner.Element.SetAttributeValue("TagValue", "6cc2a2b5-b4b3-4166-b2e6-eb7a9809fe07");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.FileName' at 74;7.", repo.NewGroupAddress.FileNameInfo, new RecordItemIndex(4));
            repo.NewGroupAddress.FileName.Click("74;7");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Dropdown' at 62;29.", repo.Dropdown.SelfInfo, new RecordItemIndex(5));
            repo.Dropdown.Self.Click("62;29");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TableLayoutPaddedTables.EnterDescription1' at 71;21.", repo.NewGroupAddress.TableLayoutPaddedTables.EnterDescription1Info, new RecordItemIndex(6));
            repo.NewGroupAddress.TableLayoutPaddedTables.EnterDescription1.Click("71;21");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test' with focus on 'NewGroupAddress.TableLayoutPaddedTables.EnterDescription1'.", repo.NewGroupAddress.TableLayoutPaddedTables.EnterDescription1Info, new RecordItemIndex(7));
            repo.NewGroupAddress.TableLayoutPaddedTables.EnterDescription1.PressKeys("test");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.CSISelect' at 9;6.", repo.NewGroupAddress.CSISelectInfo, new RecordItemIndex(8));
            repo.NewGroupAddress.CSISelect.Click("9;6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SomeLabelTag' at 93;0.", repo.NewGroupAddress.SomeLabelTagInfo, new RecordItemIndex(9));
            repo.NewGroupAddress.SomeLabelTag.Click("93;0");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SomeLabelTag' at 93;0.", repo.NewGroupAddress.SomeLabelTagInfo, new RecordItemIndex(10));
            repo.NewGroupAddress.SomeLabelTag.Click("93;0");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Open.SystemItemNameDisplay' at 38;7.", repo.Open.SystemItemNameDisplayInfo, new RecordItemIndex(11));
            repo.Open.SystemItemNameDisplay.Click("38;7");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Open.ButtonOpen' at 29;18.", repo.Open.ButtonOpenInfo, new RecordItemIndex(12));
            repo.Open.ButtonOpen.Click("29;18");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.PopupScrollContainer1' at 593;491.", repo.NewGroupAddress.PopupScrollContainer1Info, new RecordItemIndex(13));
            repo.NewGroupAddress.PopupScrollContainer1.Click("593;491");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.FormType' at 4;5.", repo.NewGroupAddress.FormTypeInfo, new RecordItemIndex(14));
            repo.NewGroupAddress.FormType.Click("4;5");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TableLayoutPaddedTables.WatermarkPosition' at 6;12.", repo.NewGroupAddress.TableLayoutPaddedTables.WatermarkPositionInfo, new RecordItemIndex(15));
            repo.NewGroupAddress.TableLayoutPaddedTables.WatermarkPosition.Click("6;12");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SubmitLibraryFile' at 47;12.", repo.NewGroupAddress.SubmitLibraryFileInfo, new RecordItemIndex(16));
            repo.NewGroupAddress.SubmitLibraryFile.Click("47;12");
            Delay.Milliseconds(200);

        }

    }
}
