﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace madaket_XP001
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Trading_Partners recording.
    /// </summary>
    [TestModule("2ff3018c-23e0-40cf-829d-231ac7ca251e", ModuleType.Recording, 1)]
    public partial class Trading_Partners : ITestModule
    {
        /// <summary>
        /// Holds an instance of the madaket_XP001Repository repository.
        /// </summary>
        public static madaket_XP001Repository repo = madaket_XP001Repository.Instance;

        static Trading_Partners instance = new Trading_Partners();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Trading_Partners()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Trading_Partners Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", "8.3")]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.DivTagTradingPartners' at 37;12.", repo.NewGroupAddress.DivTagTradingPartnersInfo, new RecordItemIndex(0));
            repo.NewGroupAddress.DivTagTradingPartners.Click("37;12");
            Delay.Milliseconds(200);
            
            Report.Screenshot(ReportLevel.Info, "User", "", null, false, new RecordItemIndex(1));
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.NewTradingPartner' at 48;11.", repo.NewGroupAddress.NewTradingPartnerInfo, new RecordItemIndex(2));
            repo.NewGroupAddress.NewTradingPartner.Click("48;11");
            Delay.Milliseconds(200);
            
            Report.Screenshot(ReportLevel.Info, "User", "", null, false, new RecordItemIndex(3));
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.PopupScrollContainer' at 433;485.", repo.NewGroupAddress.PopupScrollContainerInfo, new RecordItemIndex(4));
            repo.NewGroupAddress.PopupScrollContainer.Click("433;485");
            Delay.Milliseconds(200);
            
            Report.Screenshot(ReportLevel.Info, "User", "", null, false, new RecordItemIndex(5));
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.AddOrUpdateTradingPartnerButton' at 56;11.", repo.NewGroupAddress.AddOrUpdateTradingPartnerButtonInfo, new RecordItemIndex(6));
            repo.NewGroupAddress.AddOrUpdateTradingPartnerButton.Click("56;11");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.EnterATradingPartnerName' at 81;5.", repo.NewGroupAddress.EnterATradingPartnerNameInfo, new RecordItemIndex(7));
            repo.NewGroupAddress.EnterATradingPartnerName.Click("81;5");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.Name' at 81;12.", repo.NewGroupAddress.NameInfo, new RecordItemIndex(8));
            repo.NewGroupAddress.Name.Click("81;12");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test_automation_partner_temp' with focus on 'NewGroupAddress.Name'.", repo.NewGroupAddress.NameInfo, new RecordItemIndex(9));
            repo.NewGroupAddress.Name.PressKeys("test_automation_partner_temp");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.PopupScrollContainer' at 434;483.", repo.NewGroupAddress.PopupScrollContainerInfo, new RecordItemIndex(10));
            repo.NewGroupAddress.PopupScrollContainer.Click("434;483");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.AddOrUpdateTradingPartnerButton' at 49;19.", repo.NewGroupAddress.AddOrUpdateTradingPartnerButtonInfo, new RecordItemIndex(11));
            repo.NewGroupAddress.AddOrUpdateTradingPartnerButton.Click("49;19");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TestAutomationTradingPartner' at 77;10.", repo.NewGroupAddress.TestAutomationTradingPartnerInfo, new RecordItemIndex(12));
            repo.NewGroupAddress.TestAutomationTradingPartner.Click("77;10");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.DivTagTradingPartners' at 52;9.", repo.NewGroupAddress.DivTagTradingPartnersInfo, new RecordItemIndex(13));
            repo.NewGroupAddress.DivTagTradingPartners.Click("52;9");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TradingPartnerseSearch' at 132;6.", repo.NewGroupAddress.TradingPartnerseSearchInfo, new RecordItemIndex(14));
            repo.NewGroupAddress.TradingPartnerseSearch.Click("132;6");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test_automation_partner' with focus on 'NewGroupAddress.TradingPartnerseSearch'.", repo.NewGroupAddress.TradingPartnerseSearchInfo, new RecordItemIndex(15));
            repo.NewGroupAddress.TradingPartnerseSearch.PressKeys("test_automation_partner");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TestAutomationPartnerTemp' at 62;4.", repo.NewGroupAddress.TestAutomationPartnerTempInfo, new RecordItemIndex(16));
            repo.NewGroupAddress.TestAutomationPartnerTemp.Click("62;4");
            Delay.Milliseconds(200);
            
            //Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TestAutomationTradingPartner1' at 124;5.", repo.NewGroupAddress.TestAutomationTradingPartner1Info, new RecordItemIndex(17));
            //repo.NewGroupAddress.TestAutomationTradingPartner1.Click("124;5");
            //Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.HttpsEdiAlphaMadakethealthComServ1' at 11;12.", repo.NewGroupAddress.HttpsEdiAlphaMadakethealthComServ1Info, new RecordItemIndex(18));
            repo.NewGroupAddress.HttpsEdiAlphaMadakethealthComServ1.Click("11;12");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.ConfirmValue' at 92;8.", repo.MadaketHealth.ConfirmValueInfo, new RecordItemIndex(19));
            repo.MadaketHealth.ConfirmValue.Click("92;8");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.ConfirmAction' at 47;11.", repo.MadaketHealth.ConfirmActionInfo, new RecordItemIndex(20));
            repo.MadaketHealth.ConfirmAction.Click("47;11");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TradingPartnerseSearch' at 146;13.", repo.NewGroupAddress.TradingPartnerseSearchInfo, new RecordItemIndex(21));
            repo.NewGroupAddress.TradingPartnerseSearch.Click("146;13");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'test_automation_partner' with focus on 'NewGroupAddress.TradingPartnerseSearch'.", repo.NewGroupAddress.TradingPartnerseSearchInfo, new RecordItemIndex(22));
            repo.NewGroupAddress.TradingPartnerseSearch.PressKeys("test_automation_partner");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.SearchSubmitIcon' at 1;13.", repo.NewGroupAddress.SearchSubmitIconInfo, new RecordItemIndex(23));
            repo.NewGroupAddress.SearchSubmitIcon.Click("1;13");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MadaketHealth.NoSubmission' at 45;9.", repo.MadaketHealth.NoSubmissionInfo, new RecordItemIndex(24));
            repo.MadaketHealth.NoSubmission.Click("45;9");
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'NewGroupAddress.TradingPartnerseSearch' at 57;10.", repo.NewGroupAddress.TradingPartnerseSearchInfo, new RecordItemIndex(25));
            repo.NewGroupAddress.TradingPartnerseSearch.Click("57;10");
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
