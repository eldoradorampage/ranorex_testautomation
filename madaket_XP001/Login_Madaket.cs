﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;
using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace madaket_XP001
{
    /// <summary>
    /// Description of Iteration34.
    /// </summary>
    [TestModule("3B2F7411-7181-4326-9A6D-704DC23459CD", ModuleType.UserCode, 1)]
    public class Login_Madaket : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Login_Madaket()
        {
            // Do not delete - a parameterless constructor is required!
        }


        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
        
        public static void Login()
        {
        	try
        	{
        		var repo = madaket_XP001.madaket_XP001Repository.Instance;
        		Report.Info("Login Credentials: " + "https://edi-alpha.madakethealth.com/services/logout" + " (IE)");
        		Host.Current.OpenBrowser("https://edi-alpha.madakethealth.com/services/logout", "IE", "", false, false, false, false, false);
        		
        		repo.MadaketHealth.txtUsername.Click();
        		repo.MadaketHealth.txtUsername.Element.SetAttributeValue("Value", "jploumis@madakethealth.com");
        		repo.MadaketHealth.txtPassword.Element.SetAttributeValue("Value", "Monkey1213!");
        		
        		Validate.Exists(repo.MadaketHealth.btnLoginInfo);
        		
        		repo.MadaketHealth.btnLogin.Click();
        	}
        	catch (Exception ex)
        	{
        		Report.Warn("Login Exception: " + ex.Message);
        	}
        }
    }
}
